import java.util.Random;
class Deck {
	public int numOfCards;
	public Card [] stack;
	Random rng = new Random();
	
	public Deck() {
		int [] ranks = new int [] {1,2,3,4,5,6,7,8,9,10,11,12,13};
		String [] suits = new String [] {"Hearts", "Diamonads", "Clubs", "Spades"};
		this.stack = new Card [52];
		int i = 0;
		for(int j = 0; j<ranks.length; j++) {
			for(int k = 0; k<suits.length; k++) {
				this.stack[i] = new Card (ranks[j], suits[k]);
				i++;
			}
		}
	}
	
	public int lenght() {
		return this.numOfCards;
	}
	
	public Card drawTopCard() {
		Card cardPicked = this.stack[stack.length-1];
		Card [] placeHolder = stack;
		this.numOfCards = this.numOfCards - 1;
		this.stack = new Card[stack.length-1];
		for(int i = 0; i<stack.length;i++) {
			stack[i]=placeHolder[i];
		}
		return cardPicked;
	}
	
	public String toString() {
		String s = "";
		for(Card c : this.stack) {
			s+= c.toString() + "\n";
		}
		return s;
	}
	
	public void shuffle() {
		for(int i = 0; i<this.stack.length; i++) {
			int ranNum = rng.nextInt(52);
			Card cardSwtich = this.stack[i];
			this.stack[i] = this.stack[ranNum];
			this.stack[ranNum] = cardSwtich;
		}
	}
}