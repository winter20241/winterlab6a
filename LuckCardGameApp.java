import java.util.Scanner;
class LuckCardGameApp {
	public static void main(String args []) {
		Scanner reader = new Scanner(System.in);
		
		Deck d1 = new Deck();
		
		d1.shuffle();
		
		System.out.println("Welcome to a game");
		System.out.println("How many cards would you like to remove");
		int numToTakeOut = reader.nextInt();
		
		while(numToTakeOut > 52) {
			System.out.println("That's too many, try again");
			numToTakeOut = reader.nextInt();
		}
		
		System.out.println(d1);
		
		for(int i = 0; i<numToTakeOut;i++) {
			d1.drawTopCard();
		}
		
		System.out.println("Here is your new deck of cards");
		System.out.println(d1);
	}
}